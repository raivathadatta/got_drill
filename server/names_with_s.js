import got from "../data/got.js";
export default function nameWithS(data = { ...got }) {
    try {
        let nameWithS = []
        nameWithS = data['houses'].reduce((accumilater, house) => {
            accumilater.push(house['people'].reduce((accumilaterPerson, person) => {
                if (person['name'].includes('s') || person['name'].includes('S')) { accumilaterPerson.push(person['name']) }
                return accumilaterPerson
            }, []))
            return accumilater
        }, [])
        nameWithS = nameWithS.flat()
        return nameWithS
    } catch (err) {
        console.log(err)
    }
}
nameWithS()