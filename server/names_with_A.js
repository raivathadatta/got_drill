import got from "../data/got.js";

export default function namesWithA(data = { ...got }) {
    try {
        let namesWithA = []
        namesWithA = data['houses'].reduce((accumilater, house) => {
            accumilater.push(house['people'].reduce((accumilaterPerson, person) => {
                if (person['name'].includes('A') || person['name'].includes('a')) {
                    accumilaterPerson.push(person['name'])
                }
                return accumilaterPerson
            }, []))
            return accumilater
        }, [])
        return namesWithA.flat()
    } catch (err) {
        console.log(err)
    }
}