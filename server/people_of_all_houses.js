import got from "../data/got.js";

export default function peopleNameOfAllHouses(data = { ...got }) {
    let peopleOfAllHouses = []
    try {
        peopleOfAllHouses = data['houses'].reduce((accumilater, house) => {
           
            accumilater.push([house['name'] , house['people'].reduce((accumilaterPerson, person) => {
                accumilaterPerson.push(person['name'])
                return accumilaterPerson
            }, [])])
            return accumilater
        }, [])
      
    }
    catch (err) {
        console.log(err.toString())
    }finally{
        console.log(peopleOfAllHouses.sort())
        return Object.fromEntries(peopleOfAllHouses)
    }
    
}