import got from "../data/got.js";

export default function surNamesWithA(data = { ...got }) {

    try {
        let surNamesWithA = data['houses'].reduce((accumilater, house) => {
            accumilater.push(house['people'].reduce((accumilaterPerson, person) => {
                if (person['name'].split(" ")[1].charAt(0) == 'A') {
                    accumilaterPerson.push(person['name'])
                }
                return accumilaterPerson
            }, []))
            return accumilater
        }, [])
        return surNamesWithA.flat()
    } catch (err) {
        console.log(err)
    }
}