import got from "../data/got.js";

export default function everyOne(data = { ...got }) {
    try {
        let everyOneInHouse = []
        everyOneInHouse = data['houses'].reduce((accumilater, house) => {
            house['people'].forEach((person) => {
                accumilater.push(person['name'])
            })
            return accumilater
        }, []);
        return everyOneInHouse
    } catch (err) {
        console.log(err.message)
    }
}