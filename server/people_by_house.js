
import got from "../data/got.js";

export default function peopleByHouses(data = { ...got }) {
    try {
        let countOfPeopleInHouse = []
        countOfPeopleInHouse=   
            data['houses'].reduce((accumilater,house) => {
                accumilater.push([house['name'], house['people'].length])
                return accumilater
        },[])
        countOfPeopleInHouse = Object.fromEntries(countOfPeopleInHouse.sort())
        return countOfPeopleInHouse
    } catch (err) {
        console.log(err)
    }
}