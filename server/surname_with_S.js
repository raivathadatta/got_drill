import got from "../data/got.js";

export default function surnameWithS(data = { ...got }) {
    try {
        let surNamesWithS = data['houses'].reduce((accumilater, house) => {
            accumilater.push(house['people'].reduce((accumilaterPerson, person) => {
                if (person['name'].split(" ")[1].charAt(0) == 'S') {
                    accumilaterPerson.push(person['name'])
                }
                return accumilaterPerson
            }, []))
            return accumilater
        }, [])
        return surNamesWithS.flat()
    }
    catch (err) {
        console.log(err)
    }
}