import got from '../data/got.js'

export default function countAllPeople(data = { ...got }) {
    try {
        return data['houses'].reduce((accumilater, house) => {
            accumilater += house['people'].length
            return accumilater
        }, 0)
    } catch (err) {
        console.log(err)
    }
}
