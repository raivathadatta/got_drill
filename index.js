import got from "./data/got.js"
let data = { ...got }
function countAllPeople() {
    try {
        return data['houses'].reduce((accumilater, house) => {
            accumilater += house['people'].length
            return accumilater
        }, 0)
    } catch (err) {
        console.log(err)
    }
}

function peopleByHouses() {
    try {
        let countOfPeopleInHouse = []
        countOfPeopleInHouse =
            data['houses'].reduce((accumilater, house) => {
                accumilater.push([house['name'], house['people'].length])
                return accumilater
            }, [])
        countOfPeopleInHouse = Object.fromEntries(countOfPeopleInHouse.sort())
        return countOfPeopleInHouse
    } catch (err) {
        console.log(err)
    }
}

function everyone() {
    try {
        let everyOneInHouse = []
        everyOneInHouse = data['houses'].reduce((accumilater, house) => {
            house['people'].forEach((person) => {
                accumilater.push(person['name'])
            })
            return accumilater
        }, []);
        return everyOneInHouse
    } catch (err) {
        console.log(err.message)
    }
}

function nameWithS() {
    try {
        let nameWithS = []
        nameWithS = data['houses'].reduce((accumilater, house) => {
            accumilater.push(house['people'].reduce((accumilaterPerson, person) => {
                if (person['name'].includes('s') || person['name'].includes('S')) { accumilaterPerson.push(person['name']) }
                return accumilaterPerson
            }, []))
            return accumilater
        }, [])
        nameWithS = nameWithS.flat()
        return nameWithS
    } catch (err) {
        console.log(err)
    }

}

function nameWithA() {
    // your code goes here
    try {
        let namesWithA = []
        namesWithA = data['houses'].reduce((accumilater, house) => {
            accumilater.push(house['people'].reduce((accumilaterPerson, person) => {
                if (person['name'].includes('A') || person['name'].includes('a')) {
                    accumilaterPerson.push(person['name'])
                }
                return accumilaterPerson
            }, []))
            return accumilater
        }, [])
        return namesWithA.flat()
    } catch (err) {
        console.log(err)
    }
}

function surnameWithS() {
    // your code goes here
    try {
        let surNamesWithS = data['houses'].reduce((accumilater, house) => {
            accumilater.push(house['people'].reduce((accumilaterPerson, person) => {
                if (person['name'].split(" ")[1].charAt(0) == 'S') {
                    accumilaterPerson.push(person['name'])
                }
                return accumilaterPerson
            }, []))
            return accumilater
        }, [])
        return surNamesWithS.flat()
    }
    catch (err) {
        console.log(err)
    }

}

function surnameWithA() {
    // your code goes here
    try {
        let surNamesWithA = data['houses'].reduce((accumilater, house) => {
            accumilater.push(house['people'].reduce((accumilaterPerson, person) => {
                if (person['name'].split(" ")[1].charAt(0) == 'A') {
                    accumilaterPerson.push(person['name'])
                }
                return accumilaterPerson
            }, []))
            return accumilater
        }, [])
        return surNamesWithA.flat()
    } catch (err) {
        console.log(err)
    }
}

function peopleNameOfAllHouses() {
    let peopleOfAllHouses = []
    try {
        peopleOfAllHouses = data['houses'].reduce((accumilater, house) => {

            accumilater.push([house['name'], house['people'].reduce((accumilaterPerson, person) => {
                accumilaterPerson.push(person['name'])
                return accumilaterPerson
            }, [])])
            return accumilater
        }, [])

    }
    catch (err) {
        console.log(err.toString())
    } finally {
        return Object.fromEntries(peopleOfAllHouses)
    }
}

// Testing your result after writing your function
console.log(countAllPeople());
// Output should be 33

console.log(peopleByHouses());
// Output should be
//{Arryns: 1, Baratheons: 6, Dothrakis: 1, Freys: 1, Greyjoys: 3, Lannisters: 4,Redwyne: 1,Starks: 8,Targaryens: 2,Tullys: 4,Tyrells: 2}

console.log(everyone());
// Output should be
//["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "King Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy", "Margaery (Tyrell) Baratheon", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn", "Edmure Tully", "Brynden Tully", "Olenna (Redwyne) Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

console.log(nameWithS(), 'with s');
// Output should be
// ["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "Stannis Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn"]

console.log(nameWithA());
// Output should be
// ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon", "Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Yara Greyjoy", "Margaery Baratheon", "Loras Tyrell", "Catelyn Stark", "Lysa Arryn", "Olenna Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

console.log(surnameWithS(), 'surname with s');
// Output should be
// ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow", "Catelyn Stark"]

console.log(surnameWithA());
// Output should be
// ["Lysa Arryn", "Jon Arryn"]

console.log(peopleNameOfAllHouses());
// Output should be
// {Arryns: ["Jon Arryn"], Baratheons: ["Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon"], Dothrakis: ["Khal Drogo"], Freys: ["Walder Frey"], Greyjoys: ["Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy"], Lannisters: ["Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon"], Redwyne: ["Olenna Tyrell"], Starks: ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow"], Targaryens: ["Daenerys Targaryen", "Viserys Targaryen"], Tullys: ["Catelyn Stark", "Lysa Arryn", "Edmure Tully", "Brynden Tully"], Tyrells: ["Margaery Baratheon", "Loras Tyrell"]}